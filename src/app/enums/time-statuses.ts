// All time tracking statuses in one place
export enum TimeStatuses {
  DONE = 'DONE',
  RUNNING = 'RUNNING',
  READY_TO_START = 'READY_TO_START',
}
