import { Component, Input, OnChanges, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { TimeStatuses } from '../enums';
import { TimeItem } from '../types';
import { add, reset, sort } from '../store';

@Component({
  selector: 'app-time-tracker-list',
  templateUrl: './time-tracker-list.component.html',
  styleUrls: ['./time-tracker-list.component.scss']
})
export class TimeTrackerListComponent implements OnInit, OnChanges {
  @Input() trackerList: TimeItem[] | undefined | null;
  public isRestart = false;

  constructor(private store: Store<{ doneItemsState: Array<TimeItem> }>) {}

  ngOnInit(): void {
    this.addDefaultItem();
  }

  ngOnChanges(): void {
    this.store.select('doneItemsState').subscribe((val) => {
      // For not mapping and comparing old store data with new i add this flag
      if (val.length || this.isRestart) {
        this.addDefaultItem();
      }
    });
  }


  // In that component i decided to not push this default value to the store because this item have no value
  // so i added method and call it almost in every action of out tracker
  public addDefaultItem(): void {
    this.trackerList = this.trackerList?.concat([{
      status: TimeStatuses.READY_TO_START,
      time: 0,
    }]);
  }

  public addNewItem(time: number): void {
    this.store.dispatch(add({ status: TimeStatuses.DONE, time }));
  }

  public sort(): void {
    this.store.dispatch(sort());
  }

  public restart(): void {
    this.isRestart = true;
    this.store.dispatch(reset());
  }
}
