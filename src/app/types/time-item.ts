import { Status } from './status';

// Type for our every tracking row line item
export interface TimeItem {
  status: Status;
  time: number;
}
