import { TimeStatuses } from '../enums';

export type Status = TimeStatuses.DONE | TimeStatuses.READY_TO_START | TimeStatuses.RUNNING;
