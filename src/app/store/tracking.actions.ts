import { createAction, props } from '@ngrx/store';
import { TimeItem } from '../types';

export const add = createAction('[Timer] Add', props<TimeItem>());
export const sort = createAction('[Timer] Sort');
export const reset = createAction('[Timer] Reset');
