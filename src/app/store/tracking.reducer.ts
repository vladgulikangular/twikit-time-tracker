import { createReducer, on } from '@ngrx/store';
import { TimeItem } from '../types';
import { add, sort, reset } from './tracking.actions';

export const initialState: TimeItem[] = [];

const _trackerReducer = createReducer(
  initialState,
  on(add, (state, action) => [...state, action]),
  on(sort, (state) => state.slice().reverse()),
  on(reset, () => initialState)
);

export function trackerReducer(state: any, action: any) {
  return _trackerReducer(state, action);
}
