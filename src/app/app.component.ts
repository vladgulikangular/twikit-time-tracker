import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { TimeItem } from './types';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public trackerList$: Observable<TimeItem[]>;

  constructor(private store: Store<{ doneItemsState: Array<TimeItem> }>) {
    this.trackerList$ = store.select(state => state.doneItemsState);
  }
}
