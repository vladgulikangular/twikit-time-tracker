import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { StoreModule } from '@ngrx/store';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TimeTrackerListComponent } from './time-tracker-list/time-tracker-list.component';
import { TimeTrackerItemComponent } from './time-tracker-item/time-tracker-item.component';
import { trackerReducer } from './store';

@NgModule({
  declarations: [
    AppComponent,
    TimeTrackerListComponent,
    TimeTrackerItemComponent,
   ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
    StoreModule.forRoot({
      doneItemsState: trackerReducer
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
