import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { ReplaySubject, timer } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { TimeStatuses } from '../enums';
import { TimeItem } from '../types';

@Component({
  selector: 'app-time-tracker-item',
  templateUrl: './time-tracker-item.component.html',
  styleUrls: ['./time-tracker-item.component.scss']
})
export class TimeTrackerItemComponent implements OnInit {
  @Input() trackerItem: TimeItem;

  @Output() addNewItemEvent = new EventEmitter<number>();

  public destroy: ReplaySubject<any> = new ReplaySubject<any>(1);
  public statusText = '';
  public timer: number;
  public status: string;
  public isDone = false;
  public statusIcon: string;
  public statusClass: string;

  // Getting all we need for rendering proper data styles and text
  public getItemStatusInfo(): void {
      switch (true) {
        case this.trackerItem?.status === TimeStatuses.DONE:
          this.isDone = true;
          this.statusText = 'Done';
          this.statusIcon = 'done';
          this.statusClass = 'timer-btn_done';
          break;
        case this.trackerItem?.status === TimeStatuses.READY_TO_START:
          this.statusText = 'Start';
          this.statusIcon = 'play_arrow';
          this.statusClass = 'timer-btn_start';
          break;
        case this.trackerItem?.status === TimeStatuses.RUNNING:
          this.statusText = 'stop';
          this.statusIcon = 'stop';
          this.statusClass = 'timer-btn_stop';
          break;
        default:
      }
  }

  public ngOnInit(): void {
    this.getItemStatusInfo();
    this.timer = this.trackerItem.time;
    this.status = this.trackerItem.status;
  }

  public trackerActionClick(): void {
    if (!this.isDone) {
      switch (true) {
        case this.status === TimeStatuses.READY_TO_START:
          this.start();
          break;
        case this.status === TimeStatuses.RUNNING:
          this.stop();
      }
    }
  }

  public start(): void {
    this.status = TimeStatuses.RUNNING;
    this.statusText = 'Stop';
    this.statusIcon = 'stop';
    this.statusClass = 'timer-btn_stop';
    // Main counter magic goes here
    timer(0, 1000).pipe(takeUntil(this.destroy)).subscribe((val: number) => {
      this.timer = val;
    });
  }

  // Closing our Observable with timer here and adding item with 'DONE' status in out store
  public stop(): void {
    this.destroy.next();
    this.destroy.complete();
    this.addNewItemEvent.emit(this.timer);
  }
}
